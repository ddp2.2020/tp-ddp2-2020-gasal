import java.util.Scanner;
import java.util.Arrays;

public class TP1_3 {
    public static void main(String args[]) {
        System.out.println("Selamat datang di bank Dedepedua!");
        Scanner sc= new Scanner(System.in);
        int harga = 200000;
        String listOfTipe[] = new String[] {"gold", "silver", "bronze"};
        int[] listOfDiskon= {25, 15, 5};
        System.out.print("Apa tipe kartu anda?: ");
        String tipe = sc.nextLine();
        System.out.print("Sejak tahun berapa anda menjadi member?: ");
        int tahun= Integer.parseInt(sc.nextLine());
        int diskon = 0;
        if(tahun <= 2017){
        diskon = 5;
       }
        int totalDiskon = 0;
        int totalHarga = 0;
        try {
        int index = Arrays.asList(listOfTipe).indexOf(tipe.toLowerCase());
        diskon +=  listOfDiskon[index];
        totalDiskon = diskon / 100 * harga;
        totalHarga= harga - totalDiskon;
       } catch (Exception e) {
        totalDiskon= diskon / 100 * harga;
        totalHarga= harga - diskon;
        }
        System.out.println("Dapat sebanyak " + diskon + " persen");
        System.out.print("Harga barang menjadi " + totalHarga);
     }  
}
