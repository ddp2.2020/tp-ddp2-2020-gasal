import java.util.Scanner;
import java.util.Arrays;

public class TP1_2 {
    public static void main(String args[]) {
        System.out.println("Selamat datang di DDP2");
        Scanner sc= new Scanner(System.in);
        System.out.print("Nama fakultas apa yang anda ingin ketahui?: ");
        String kata = sc.nextLine(); 
        System.out.println(kata);
        String singkatans[] = new String[] {"fk", "feb", "fh", "ft","ff" ,"fpsi", "fasilkom", "fkg", "fik", "fkm", "fib", "fisip", "fmipa"};
        String kepanjangans[] = new String[] {"Fakultas Kedokteran", "Fakultas Ekomoni dan Bisnis", "Fakultas Hukum", "Fakultas Teknik", "Fakultas Farmasi", "Fakultas Psikologi", "Fakultas Ilmu Komputer", "Fakultas Kedokteran Gigi", "Fakultas Ilmu Keperawatan", "Fakultas Kesehatan Masyarakat", "Fakultas Ilmu Pengetahuan Budaya", "Fakultas Ilmu Sosial dan Ilmu Politik", "Fakultas Matematika dan Ilmu Pengetahuan Alam" };

        try {
        int index = Arrays.asList(singkatans).indexOf(kata.toLowerCase());
        System.out.print(kepanjangans[index]);
        } catch (Exception e) {
        System.out.print("Fakultas tidak ditemukan");
        }


    }  

}
