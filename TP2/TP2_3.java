import java.util.Scanner;

import javax.sound.midi.SysexMessage;

public class TP2_3 {
    public static void main(String args[]) {
        System.out.println("Selamat datang di desa  Dedepedua");
        Scanner sc = new Scanner(System.in);
        System.out.print("Apa hewan kesukaan kepala desa? :  ");
        String hewan = sc.nextLine();
        System.out.print("Berapa jumlah harimau di desa Dedepedua? : ");
        int harimau = Integer.parseInt(sc.nextLine());
        System.out.print("Berapa kecepatan harimau di desa Dedepedua? : ");
        double kecepatan = Double.parseDouble(sc.nextLine());

        if (hewan.toLowerCase().equals("burung hantu")) {
            if (harimau == 8 || harimau == 10 || harimau == 18) {
                if (kecepatan >= 100 && kecepatan <= 120) {
                    System.out.print("Selamat anda boleh masuk");
                    return;
                }
            }
        }
        System.out.print("Kamu dilarang masuk");

    }

}
