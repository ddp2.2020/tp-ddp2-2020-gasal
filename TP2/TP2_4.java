import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class TP2_4 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);

        String nama[] = { "a", "b", "c" };
        int angka[] = new int[3];
        Integer[] angka2 = new Integer[3];

        for (int i = 0; i < 3; i++) {
            System.out.print(nama[i] + ": ");
            angka[i] = sc.nextInt();
            angka2[i] = angka[i];
        }
        Arrays.sort(angka);
        int index = Arrays.asList(angka2).indexOf(angka[1]);
        String pemimpin = nama[index];
        int powerOfa = (int) Math.pow(angka[0], 2);
        int powerOfb = (int) Math.pow(angka[1], 2);
        int powerOfc = (int) Math.pow(angka[2], 2);

        if (powerOfa + powerOfb == powerOfc) {
            System.out.println("Segitiga siku-siku, mereka penyusup");
            System.out.println(pemimpin + " pemimpin mereka");
        } else {
            System.out.println("Bukan segitiga siku-siku, mereka bukan penyusup");

        }
    }

}
